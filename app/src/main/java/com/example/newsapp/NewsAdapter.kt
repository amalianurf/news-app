package com.example.newsapp

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import java.text.SimpleDateFormat
import java.util.*

class NewsAdapter(private val articles: MutableList<Article>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val VIEW_TYPE_TITLE = 0
    private val VIEW_TYPE_LATEST_NEWS = 1
    private val VIEW_TYPE_NORMAL_NEWS = 2

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            VIEW_TYPE_TITLE -> {
                val view = LayoutInflater.from(parent.context).inflate(R.layout.item_title, parent, false)
                TitleViewHolder(view)
            }
            VIEW_TYPE_LATEST_NEWS -> {
                val view = LayoutInflater.from(parent.context).inflate(R.layout.item_latest_news, parent, false)
                LatestNewsViewHolder(view)
            }
            VIEW_TYPE_NORMAL_NEWS -> {
                val view = LayoutInflater.from(parent.context).inflate(R.layout.item_news, parent, false)
                NormalNewsViewHolder(view)
            }
            else -> throw IllegalArgumentException("Invalid view type")
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder.itemViewType) {
            VIEW_TYPE_TITLE -> {
                val titleViewHolder = holder as TitleViewHolder
                if (position == 0) {
                    titleViewHolder.bindTitle("Berita Terkini")
                } else {
                    titleViewHolder.bindTitle("Semua Berita")
                }
            }
            VIEW_TYPE_LATEST_NEWS -> {
                val latestNewsViewHolder = holder as LatestNewsViewHolder
                latestNewsViewHolder.bindLatestNews(articles[position - 1])
            }
            VIEW_TYPE_NORMAL_NEWS -> {
                val normalNewsViewHolder = holder as NormalNewsViewHolder
                normalNewsViewHolder.bindNormalNews(articles[position - 1])
            }
        }
    }

    override fun getItemCount(): Int {
        return articles.size + 1
    }

    override fun getItemViewType(position: Int): Int {
        return when (position) {
            0 -> VIEW_TYPE_TITLE
            1 -> VIEW_TYPE_LATEST_NEWS
            2 -> VIEW_TYPE_TITLE
            else -> VIEW_TYPE_NORMAL_NEWS
        }
    }

    fun addArticles(newArticles: List<Article>) {
        articles.addAll(newArticles)
        notifyDataSetChanged()
    }

    inner class TitleViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val titleTextView: TextView = itemView.findViewById(R.id.titleTextView)

        fun bindTitle(title: String) {
            titleTextView.text = title
        }
    }

    inner class LatestNewsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val latestNewsImage: ImageView = itemView.findViewById(R.id.latestNewsImage)
        private val latestNewsTitle: TextView = itemView.findViewById(R.id.latestNewsTitle)
        private val latestNewsAuthor: TextView = itemView.findViewById(R.id.latestNewsAuthor)
        private val latestNewsDate: TextView = itemView.findViewById(R.id.latestNewsDate) // convert date

        fun bindLatestNews(article: Article) {
            latestNewsTitle.text = article.title
            latestNewsAuthor.text = article.author ?: "Unknown Author"
            latestNewsDate.text = parseDate(article.publishedAt)

            Glide.with(itemView.context)
                .load(article.urlToImage)
                .into(latestNewsImage)
        }
    }

    inner class NormalNewsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val newsImage: ImageView = itemView.findViewById(R.id.newsImage)
        private val newsTitle: TextView = itemView.findViewById(R.id.newsTitle)
        private val newsAuthor: TextView = itemView.findViewById(R.id.newsAuthor)
        private val newsDate: TextView = itemView.findViewById(R.id.newsDate) // convert date

        fun bindNormalNews(article: Article) {
            newsTitle.text = article.title
            newsAuthor.text = article.author ?: "Unknown Author"
            newsDate.text = parseDate(article.publishedAt)

            Glide.with(itemView.context)
                .load(article.urlToImage)
                .into(newsImage)
        }
    }

    fun parseDate(inputDate: String): String {
        val inputFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.getDefault())
        val outputFormat = SimpleDateFormat("MMM dd, yyyy", Locale.getDefault())

        return try {
            val date = inputFormat.parse(inputDate)
            outputFormat.format(date ?: Date())
        } catch (e: Exception) {
            e.printStackTrace()
            ""
        }
    }
}