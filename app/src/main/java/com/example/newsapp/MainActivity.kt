package com.example.newsapp

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class MainActivity : AppCompatActivity() {
    private lateinit var newsAdapter: NewsAdapter
    private lateinit var layoutManager: LinearLayoutManager

    private var articles: MutableList<Article> = mutableListOf()
    private var currentPage = 1
    private var isLoading = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        layoutManager = LinearLayoutManager(this)
        newsAdapter = NewsAdapter(articles)

        val recyclerView: RecyclerView = findViewById(R.id.recyclerView)
        recyclerView.layoutManager = layoutManager
        recyclerView.adapter = newsAdapter

        loadNews()

        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)

                val visibleItemCount = layoutManager.childCount
                val totalItemCount = layoutManager.itemCount
                val firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition()

                if (!isLoading && (visibleItemCount + firstVisibleItemPosition) >= totalItemCount &&
                    firstVisibleItemPosition >= 0) {
                    currentPage++
                    loadNews()
                }
            }
        })
    }

    fun filterArticlesWithImages(articles: List<Article>): List<Article> {
        val filteredArticles = mutableListOf<Article>()
        for (article in articles) {
            if (!article.urlToImage.isNullOrEmpty()) {
                filteredArticles.add(article)
            }
        }
        return filteredArticles
    }

    private fun loadNews() {
        isLoading = true
        val retrofit = Retrofit.Builder()
            .baseUrl("https://newsapi.org/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        val newsService = retrofit.create(NewsService::class.java)
        val call = newsService.getTopHeadlines("us", "66bb66fdefbf4c3ab0e40a651c5fc23a",
            currentPage, 10)

        call.enqueue(object : Callback<NewsResponse> {
            override fun onResponse(call: Call<NewsResponse>, response: Response<NewsResponse>) {
                if (response.isSuccessful && response.body() != null) {
                    val newArticles = response.body()!!.articles
                    val filteredArticles = filterArticlesWithImages(newArticles)
                    newsAdapter.addArticles(filteredArticles)
                }

                isLoading = false
            }

            override fun onFailure(call: Call<NewsResponse>, t: Throwable) {
                Log.e("MainActivity", "Error loading news", t)
                isLoading = false
            }
        })
    }
}